Feature: Search for the product

  ### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
  ### Available products: "apple", "mango", "tofu", "water"
  ### Prepare Positive and negative scenarios
  Scenario Outline: Verify the search result for valid keywords
    When he search for "<SearchKey>"
    Then he sees the status code "<StatusCode>"
    And he sees the results displayed for "<SearchResult>"

    Examples: 
      | SearchKey | StatusCode | SearchResult              |
      | apple     |        200 | Kanzi Appels 1kg          |
      | mango     |        200 | Topo Chico Tropical mango |

  Scenario Outline: Verify the search result for invalid keywords
    When he search for "<SearchKey>"
    Then he sees the status code "<StatusCode>"
    Then he doesn not see the results

    Examples: 
      | SearchKey | StatusCode |
      | car       |        404 |
      | bike      |        404 |
