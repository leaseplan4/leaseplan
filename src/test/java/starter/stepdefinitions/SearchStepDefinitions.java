package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import page.WaarkoopPage;

public class SearchStepDefinitions {

    @Steps
    public WaarkoopPage waarkoopPage;

    @When("he search for {string}")
    public void heSearchFor(String keyword) {
    	waarkoopPage.searchFor(keyword);
    }
    
    @Then("he sees the status code {string}")
    public void heSeesTheStatusCode(String statusCode) {
    	waarkoopPage.verifyStatusCode(statusCode);    	 
    }
    
    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedFor(String result) {
    	waarkoopPage.verifyHasItem(result);        
    }
    
    @Then("he doesn not see the results")
    public void heDoesnNotSeeTheResults() {
    	waarkoopPage.verifyErrorShown();
    }
}
