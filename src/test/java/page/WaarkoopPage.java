package page;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class WaarkoopPage {
	
	String baseUrl = "https://waarkoop-server.herokuapp.com";
	String endPoint = "/api/v1/search/test/";

	@Step
	public void searchFor(String keyword) {
		SerenityRest.given().get(baseUrl + endPoint + keyword);
	}

	@Step
	public void verifyStatusCode(String statusCode) {		
		restAssuredThat(response -> response.statusCode( Integer.parseInt(statusCode)));		
	}

	@Step
	public void verifyHasItem(String result) {		
		restAssuredThat(response -> response.body("title", hasItem(result)));		
	}

	@Step
	public void verifyErrorShown() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));		
	}
}
