# Feature file modification
    1) Created positive and negative scenarios
    2) Implemented data driven approach using Scenario Outline and Examples keyword
    3) Added more validations for status code

# Step definitions
    1) Assertions and moved to seperate class file to integrate with @Step annotations
    2) Modified the validations to increase stability in test execution

# Reporting
    1) Enabled Serenity report and the report will generate at /target/site/serenity/index.html
    2) Command to run : mvn clean verify

# GitLab Repository and CI/CD
    1) Commited and pushed all the required files
    2) Created .gitlab-ci.yml file where the maven scripts are added

# Configure CI/CD using gitlab runner
    1) Install gitlab runner
        a) Download the gitlab runner from gitlab website and paste the exe in your system folder
        b) Open a command prompt and navigate to the folder where the exe is placed
        c) Run the below command to install the exe
            xxxxx.exe install
    2) Register the runner
        a) Run the below command to register the gitlab runner
            xxxxx.exe register
        b) You have to enter the below details as part of the registration process
            gitlab-ci coordinator url : https://gitlab.com/
            gitlab-ci token : you can copy it from gitlab -> project -> settings -> CI/CD -> Runner
            gitlab-ci description : Give any description
            gitlab-ci tags : Give some tags which we will use later
            executor : shell
    3) Start runner
        a) Start the runner using the below command
            xxxxx.exe start

Now make some changes in your project local repository and commit and push the code to gitlab repository the runner will automatically call the CI/CD pipleine and the pipeline will start execution.
